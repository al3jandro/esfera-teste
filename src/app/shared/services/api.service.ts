import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from './../../../environments/environment.prod';
import { map } from 'rxjs/operators';

const url: string = environment.url;

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private data$: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(private http: HttpClient) {}

  getData(): Observable<any> {
    return this.data$.asObservable();
  }
  setData(items: any) {
    this.data$.next(items);
  }

  getUser(): Observable<any> {
    return this.http.get<any>(url).pipe(
      map((res) => {
        this.setData(res);
        return res;
      })
    );
  }

  toSlug(str: string) {
    str = str.replace(/^\s+|\s+$/g, '');
    str = str.toLowerCase();
    const from = 'àáãäâèéëêìíïîòóöôùúüûñç·/_,:;';
    const to = 'aaaaaeeeeiiiioooouuuunc------';
    for (let i = 0, l = from.length; i < l; i++) {
      str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }
    str = str
      .replace(/[^a-z0-9 -]/g, '')
      .replace(/\s+/g, '-')
      .replace(/-+/g, '-');
    return str;
  }
}
