# Teste Esfera

O Projeto foi desenvolvido com Angular na sua versão 9 e o framework do estilo MDBootstrap

## Instalar Dependências

Execute o comando `npm install` para instalar todas as bibliotecas do projeto

## Servidor de Desenvolvimento

Execute o comando `ng serve` para iniciar o servidor e navegue em sua browser no link `http://localhost:4200/`.

## Deploy do Projeto

Execute o comando `ng build --prod` para compilar o projeto.
