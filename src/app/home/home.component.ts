import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ApiService } from '../shared/services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  items: Observable<any[]>;

  constructor(private router: Router, private api: ApiService) {}

  ngOnInit() {
    this.items = this.api.getUser();
  }

  goToSingle(name: string) {
    this.router.navigate([this.api.toSlug(name)]);
  }
}
