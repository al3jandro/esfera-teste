import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.scss'],
})
export class SingleComponent implements OnInit {
  slug: Observable<string>;
  item: Observable<any>;
  constructor(
    private router: Router,
    private api: ApiService,
    private act: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.slug = this.act.paramMap.pipe(map((paramsMap) => paramsMap.get('id')));
    this.slug.subscribe((user) => {
      const value = this.api.getData();
      if (value) {
        this.item = value.pipe(
          map(
            (res) => res.filter((row) => this.api.toSlug(row.name) == user)[0]
          )
        );
      }
    });
  }

  goToHome() {
    this.router.navigate(['']);
  }
}
